package controller;

import dao.ContractDao;
import model.Contract;

import java.util.ArrayList;
import java.util.Scanner;

public class ContractController {
    private Contract contract;
    private ContractDao cd;
    private Scanner input;
    public ContractController() {
        contract = new Contract();
        cd = new ContractDao();
        input = new Scanner(System.in);
    }
    public void insertContractType(){
        System.out.println("Please Define Contract type");
        String type=input.next();
        contract.setType(type);
        if (cd.insertCONTRACT(contract)){
            System.out.println("Done");
        }else{
            System.out.println("Failed!");
        }
    }
    public void getAll(){
        ArrayList<Contract> c=cd.getAllCONTRACT();
        if (c.size()!=0){
            for (Contract ct:c) {
                System.out.println("id:{"+ct.getId()+"} ContractType:{"+ct.getType()+"}");
            }
        }else {
            System.out.println("something goes wrong!");
        }
    }
    public void getContractById(){
        Integer id;
        System.out.println("please Enter ID of Contract you want to select:");
        id= input.nextInt();
        Contract contract1=cd.getCONTRACT(id);
        if (contract1!=null){
            System.out.println("Here is Info you looking for:\n"+"ID: {"+contract1.getId()+"} ContractType: {"+contract1.getType()+"}");
        }else{
            System.out.println("Something goes wrong!");
        }
    }
    public void updateContract(){
        System.out.println("please Enter the ID of Contract you want to change:");
        Integer id=input.nextInt();
        System.out.println("Please change the Contract:");
        String contractType=input.next();
        contract.setId(id);
        contract.setType(contractType);
        if (cd.updateCONTRACT(contract)){
            System.out.println("Done");
        }else{
            System.out.println("something goes wrong!");
        }
    }
    public void deleteContract(){
        System.out.println("Enter ID to Delete Contract:");
        Integer id=input.nextInt();
        if (cd.deleteCONTRACT(id)){
            System.out.println("Done");
        }else{
            System.out.println("Something goes wrong!");
        }
    }
}
