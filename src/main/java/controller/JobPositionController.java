package controller;

import dao.JobPositionDao;
import model.JobPosition;

import java.util.ArrayList;
import java.util.Scanner;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class JobPositionController {
    public static Integer currentJobId;
    private JobPosition jobPosition;
    private JobPositionDao jobPositionDao;
    private Scanner in;

    public JobPositionController() {
        in = new Scanner(System.in);
        jobPositionDao = new JobPositionDao();
        jobPosition = new JobPosition();
    }

    public void getJobPoisitionById() {

        int id = in.nextInt();
        JobPosition j = jobPositionDao.getJOB_POSITION(id);
        if (j != null) {

            currentJobId=j.getId();
            System.out.println("ID: " + j.getId() + " Title: " + j.getTitle() + " Describtion: " + j.getDesc()  + " ContractID: " + j.getContract_id() + " CompanyID: " + j.getCompany_id() + " jobCategoryID: " + j.getJobCategoryId());
        } else {

            System.out.println("Ops!");
        }

    }

    public void InsertJobPosition() {
        System.out.println("Insert job info\n");
        System.out.println("Title:");
        jobPosition.setTitle(in.nextLine());
        System.out.println("describtion:");
        jobPosition.setDesc(in.nextLine());
        jobPosition.setCompany_id(CompanyController.currentCompanyID);
        System.out.println("Select JobCategory id:");
        new CategoryController().getAllJobCategory();
        jobPosition.setJobCategoryId(in.nextInt());
        System.out.println("select categoryType:");
        new ContractController().getAll();
        jobPosition.setContract_id(in.nextInt());

        if (jobPositionDao.insertJOB_POSITION(jobPosition)) {
            System.out.println("Done");
        } else {
            System.out.println("Something goes wrong!");
        }

    }

    public void getAll() {

        ArrayList<JobPosition> jp = jobPositionDao.getAllEmploees();
        if (jp.size()!=0) {

            System.out.println("Jobs Information\n");
            for (JobPosition j : jp) {
                System.out.println("ID: " + j.getId() + " Title: " + j.getTitle() + " Describtion: " + j.getDesc() + " ContractID: " + j.getContract_id() + " CompanyID: " + j.getCompany_id() + " jobCategoryID: " + j.getJobCategoryId());
            }
        } else {
            System.out.println("something goes wrong");
        }

    }

    public void updateJobPosition() {
        System.out.println("Enter id to start chagne:");
        jobPosition.setContract_id(in.nextInt());
        System.out.println("change the title:");
        jobPosition.setTitle(in.next());

        jobPositionDao.updateJOB_POSITION(jobPosition);
    }
    public void deleteJobPosition(){
        System.out.println("Enter id of job you want to delete:");
        int id=in.nextInt();
       if (jobPositionDao.deleteJOB_POSITION(id)){
           System.out.println("Delete is Done");
       }else{
           System.out.println("Delete failed");
       }
    }
}
