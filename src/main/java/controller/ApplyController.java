package controller;

import dao.ApplyDao;
import model.Apply;

import java.util.ArrayList;
import java.util.Scanner;

public class ApplyController {
    private Apply apply;
    private ApplyDao applyDao;
    private Scanner in;

    public ApplyController() {
        apply = new Apply();
        applyDao = new ApplyDao();
        in = new Scanner(System.in);
    }
    public void insertApply(){
        System.out.println("Insert ID of job to apply:");
        new JobPositionController().getAll();
        apply.setJobPositionId(in.nextInt());
        apply.setEmployeeId(EmploeeController.currentEmployeeId);
        if (applyDao.insertApply(apply)){
            System.out.println("Done");
        }else{
            System.out.println("failed");
        }
    }
    public void getAllApplys(){
        ArrayList<Apply>a=applyDao.getAllApplys();
        if (a.size()!=0){
            for (Apply aa:a) {
                System.out.println("id: "+aa.getId()+" EmployeeID: "+aa.getEmployeeId()+" jobPositionID: "+aa.getJobPositionId());
            }
        }else {
            System.out.println("failed");
        }
    }
    public void deleteApply(){
        System.out.println("Enter ID to Delete");
        int id=in.nextInt();

        if (applyDao.deleteApply(id)){
            System.out.println("Done");
        }else{
            System.out.println("failed");
        }
    }
    public void update(){
        System.out.println("type job_id");
        apply.setJobPositionId(in.nextInt());

        if (applyDao.updateApply(apply)){
            System.out.println("Done");
        }else {
            System.out.println("failed");
        }
    }
}
