package controller;

import dao.CategoryDao;
import model.JobCategory;


import java.util.ArrayList;
import java.util.Scanner;

public class CategoryController {
    private CategoryDao categoryDao;
    private JobCategory jobCategory;
    private Scanner in;

    public CategoryController() {
        this.categoryDao = new CategoryDao();
        this.jobCategory = new JobCategory();
        in = new Scanner(System.in);
    }

    public boolean insertJobCategory(){
        System.out.println("please Insert Job category type:");
        jobCategory.setTitle(in.next());
        if (categoryDao.insertCategory(jobCategory)) {
            System.out.println("Inserting is successfully done");
            return true;
        }else{
            System.out.println("Inserting is not done please try again");
            return false;
        }
    }
    public void getAllJobCategory(){

        ArrayList<JobCategory> itr=categoryDao.getAllCategorys();
       if (itr.size()!=0) {
           for (JobCategory i : itr) {
               System.out.println(i.getId() + " " + i.getTitle());
           }
       }else {
           System.out.println("something goes wrong!");
       }


    }

    public void deleteJobRecord(){
        System.out.println("Please select id that you want to delete:");
        if( categoryDao.deleteCategory(in.nextInt())){
            System.out.println("Delete successfully Done");
        }else{
            System.out.println("Delete failed");
        }
    }
    public void updateJobCategory(){
        System.out.println("Please Enter the ID of Ctegory that you want to modify:");
        jobCategory.setId(in.nextInt());
        System.out.println("please Enter new JobCategory Title:");
        jobCategory.setTitle(in.next());
        if (categoryDao.updateCategory(jobCategory)){
            System.out.println("Update is completed");
        }else{
            System.out.println("Updating Failed!");
        }
    }

    public void findByIdJobCategory(){
        System.out.println("Please Enter 'ID' of JobCategory:");
        int id=in.nextInt();
        JobCategory jobCategory=categoryDao.getCategory(id);
        if (jobCategory!=null){
            System.out.println("Here is Category Info:"+jobCategory);
        }else{
            System.out.println("Not found");
        }
    }
}
