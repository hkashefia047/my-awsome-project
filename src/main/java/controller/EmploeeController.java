package controller;

import dao.EmploeeDao;
import lombok.Getter;
import lombok.Setter;
import model.Employee;

import java.util.ArrayList;
import java.util.Scanner;
@Getter @Setter
public class EmploeeController {
    public static int currentEmployeeId;
    private Employee employee;
    private EmploeeDao emploeeDao;
    private Scanner in;

    public EmploeeController() {
        employee = new Employee();
        emploeeDao = new EmploeeDao();
        in=new Scanner(System.in);
    }
    public void InsertEmploeeInfo(){
        System.out.println("Rgistering begin:\n\n");
        System.out.println("UserName:");
        employee.setName(in.nextLine());
        System.out.println("PassWord:");
        employee.setPassword(in.nextLine());

        System.out.println("How Old are you?");
        employee.setAge(Integer.valueOf(in.nextLine()));
        System.out.println("Email:");
        employee.setEmail(in.nextLine());
        System.out.println("Exprince:");
        employee.setExperince(in.nextLine());
        System.out.println("TEL:");
        employee.setTelephone(in.nextLine());

        employee.setJobPostionId(0);
        //job position id left
        if (emploeeDao.insertEmploee(employee)){
            System.out.println("Registration is successful");
        }else {
            System.out.println("Registration is failed");
        }
    }
    public void getAll(){
        ArrayList<Employee> e =emploeeDao.getAllEmploees();
        if (e.size()!=0){
            for (Employee x:e
                 ) {
                System.out.println("id: {"+x.getId()+"} UserName: {"+x.getName()+" } Age:{ "+x.getAge()+" } Email: {"+x.getEmail()+"} Experince: {"+x.getExperince()+"} TEL:{"+x.getTelephone()+"}");
            }
        }else{
            System.out.println("Empty DataBase!");
        }
    }
    public void LoginEmploee(){
        System.out.println("Login\nUserName:");
        String user=in.next();
        System.out.println("Password:");
        String pass=in.next();
       if (emploeeDao.getEmploeeByEmploeeNameAndPassword(user,pass)!=null){
           System.out.println("Access Granted");
           loginOperations();
       }else{
           System.out.println("Access Denied");
       }
    }
    public void loginOperations(){
        ApplyController av=new ApplyController();
        av.insertApply();
    }
    public void DeleteEmploeeRecord(){
        System.out.println("please enter id to delete:");
        int id=in.nextInt();
        if(emploeeDao.deleteEmploee(id)){
            System.out.println("Done");
        }else{
            System.out.println("this id is not in dataBase!");
        }
    }
}
