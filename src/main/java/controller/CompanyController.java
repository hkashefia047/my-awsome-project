package controller;

import dao.CompanyDao;
import model.Company;

import java.util.ArrayList;
import java.util.Scanner;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class CompanyController {
    public static Integer currentCompanyID;
    private Company company;
    private CompanyDao companyDao;
    private Scanner input;

    public CompanyController(){
        this.company=new Company();
        this.companyDao=new CompanyDao();
        this.input=new Scanner(System.in);
    }

    public void InsertCompanyInfo(){
        System.out.println("Company Name:");
        company.setName(input.nextLine());

        System.out.println("Password:");
        company.setPassword(input.nextLine());

        System.out.println("Field of Company Activity:");
        company.setField(input.nextLine());

        System.out.println("Describe your Company:");
        company.setDescribtion(input.nextLine());

        System.out.println("How many people word in this Company: ");
        company.setEmployeeCount(input.nextInt());

        if (companyDao.insertCompany(company)){
            System.out.println("Inserting Done");
        }else {
            System.out.println("Inserting Failed");
        }
    }
    public void getAll(){
        ArrayList<Company> comp =companyDao.getAllCompany();
       if (comp.size()!=0) {
           for (Company c : comp) {
               System.out.println(" id:{" + c.getId() + "} Company Name: {" + c.getName() + "} Field of activity:{" + c.getField() + "} Describtion:{" + c.getDescribtion() + "} Emploee Count:{" + c.getEmployeeCount()+"}");
           }
       }else {
           System.out.println("Data Base is Empty!");
       }
    }
    public void login(){
        System.out.println("Login\n");
        System.out.println("UserName:");
        String user=input.next();
        System.out.println("Password:");
        String pass=input.next();
        if (companyDao.getCompanyByCompanyNameAndPassword(user,pass)!=null){

            System.out.println("Access Granted ");
            LoginOperations();

        }else {
            System.out.println("Access Denied");
        }

    }

    private void LoginOperations() {
        System.out.println("press 1 to <<insert jobs>>");
        System.out.println("press 2 to <<see applys>>");
        String command=input.next();
        if (command.equals("1")) {
            JobPositionController jp = new JobPositionController();
            jp.InsertJobPosition();
        }else if (command.equals("2")){
            ApplyController view=new ApplyController();
            view.getAllApplys();
        }
    }

    public void UpdateNameAndPassword(){

        System.out.println("Enter new Name");
        company.setName(input.next());

        System.out.println("Enter new Password");
        company.setPassword(input.next());

        if (companyDao.updateCompany(company)){
            System.out.println("Update Completed");
        }else{
            System.out.println("Update Failed");
        }
    }
    public void deleteCompanyRecord(){
        System.out.println("please enter id to delete Record");
        if (companyDao.deleteCompany(input.nextInt())){
            System.out.println("Record is deleted");
        }else {
            System.out.println("Deleting Failed!");
        }
    }
    public void getByCompanyId(){
        System.out.println("Enter Company ID :");
        Integer id=input.nextInt();
         Company myid=companyDao.getCompany(id);
        if (myid!=null){
            System.out.println("here is Information:\n"+myid.getName());
        }else {
            System.out.println("not found!");
        }
    }
}
