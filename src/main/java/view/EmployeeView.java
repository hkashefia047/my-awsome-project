package view;

import controller.EmploeeController;

import java.util.Scanner;

public class EmployeeView {
    private EmploeeController view;
    private Scanner in;
    public EmployeeView() {
        view = new EmploeeController();
        in = new Scanner(System.in);
    }
    public void EmploeeEnter(){

        System.out.println(" 1 <- SIGNING UP");
        System.out.println(" 2 <- LOGIN");
        Integer command=in.nextInt();
        switch (command) {
            case 1:
                view.InsertEmploeeInfo();
                break;
            case 2:
                view.LoginEmploee();
                break;

        }
    }

}
