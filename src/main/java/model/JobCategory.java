package model;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JobCategory {
    //THIS IS JOB CATEGORY CLASS
    private Integer id;
    private String title;


}
