package model;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class JobPosition {

    private Integer id;
    private String title;
    private String desc;
    private Integer viewCount;
    private Integer jobCategoryId;
    private Integer contract_id;
    private Integer company_id;

   }
