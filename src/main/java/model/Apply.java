package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Apply {
    private int id;
    private int jobPositionId;
    private int EmployeeId;
}
