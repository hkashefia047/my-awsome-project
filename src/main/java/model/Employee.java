package model;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Employee {
    private Integer id;
    private String name;
    private String password;
    private Integer age;
    private String email;
    private String experince;
    private String telephone;
    private Integer JobPostionId;
}
