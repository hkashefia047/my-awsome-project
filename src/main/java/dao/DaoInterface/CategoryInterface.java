package dao.DaoInterface;

import model.JobCategory;

import java.util.ArrayList;

public interface CategoryInterface {
    JobCategory getCategory(int id);

    ArrayList<JobCategory> getAllCategorys();

    boolean insertCategory(JobCategory jobCategory);

    boolean updateCategory(JobCategory jobCategory);

    boolean deleteCategory(int id);
}
