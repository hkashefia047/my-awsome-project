package dao.DaoInterface;

import model.Employee;

import java.util.ArrayList;

public interface EmploeeInterface {

    Employee getEmploee(int id);

    ArrayList<Employee> getAllEmploees();

    Employee getEmploeeByEmploeeNameAndPassword(String name, String password);

    boolean insertEmploee(Employee e);

    boolean updateEmploee(Employee e);

    boolean deleteEmploee(int id);


}
