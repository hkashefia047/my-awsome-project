package dao.DaoInterface;

import model.JobPosition;

import java.util.ArrayList;
import java.util.Set;

public interface JobPositionInterface {
    JobPosition getJOB_POSITION(int id);

    ArrayList<JobPosition> getAllEmploees();

    boolean insertJOB_POSITION(JobPosition jobPosition);

    boolean updateJOB_POSITION(JobPosition jobPosition);

    boolean deleteJOB_POSITION(int id);
}
