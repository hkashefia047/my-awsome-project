package dao.DaoInterface;

import dao.DbConnector.ConnectionFactory;
import model.Company;


import java.util.ArrayList;
import java.util.Set;

public interface CompanyInterface {
    Company getCompany(int id);

    ArrayList<Company> getAllCompany();

    Company getCompanyByCompanyNameAndPassword(String name, String password);

    boolean insertCompany(Company company);

    boolean updateCompany(Company company);

    boolean deleteCompany(int id);

}
