package dao.DaoInterface;

import model.Apply;

import java.util.ArrayList;

public interface ApplyInterface {
    Apply getApply(int id);

    ArrayList<Apply> getAllApplys();

    boolean insertApply(Apply Apply);

    boolean updateApply(Apply Apply);

    boolean deleteApply(int id);
}
