package dao.DaoInterface;

import model.Contract;


import java.util.ArrayList;
import java.util.Set;

public interface ContractInterface {
    Contract getCONTRACT(int id);

    ArrayList<Contract> getAllCONTRACT();

    boolean insertCONTRACT(Contract contract);

    boolean updateCONTRACT(Contract contract);

    boolean deleteCONTRACT(int id);

}
