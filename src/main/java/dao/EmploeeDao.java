package dao;

import dao.DaoInterface.EmploeeInterface;
import dao.DbConnector.ConnectionFactory;
import model.Employee;
import controller.EmploeeController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EmploeeDao implements EmploeeInterface {
    public Employee getEmploee(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from emploee where ID=?");
            preparedStatement.setInt(1, id);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return extractemploeeFromResultSet(rs);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private Employee extractemploeeFromResultSet(ResultSet rs) throws SQLException {
        Employee employee = new Employee();
        employee.setId(rs.getInt("ID"));
        employee.setName(rs.getString("USERNAME"));
        employee.setPassword(rs.getString("PASSWORD"));
        employee.setAge(rs.getInt("AGE"));
        employee.setExperince(rs.getString("EXPERINCE"));
        employee.setEmail(rs.getString("EMAIL"));
        employee.setTelephone(rs.getString("TEL"));

        return employee;
    }

    public ArrayList<Employee> getAllEmploees() {

        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from emploee ");

            ArrayList<Employee> e = new ArrayList<>();

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Employee employee = extractemploeeFromResultSet(rs);
                e.add(employee);
            }
            return e;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Employee getEmploeeByEmploeeNameAndPassword(String name, String password) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM emploee where USERNAME=? AND PASSWORD=?");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                EmploeeController.currentEmployeeId=resultSet.getInt("ID");
                return extractemploeeFromResultSet(resultSet);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean insertEmploee(Employee e) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO emploee(USERNAME,PASSWORD,AGE,EMAIL,EXPERINCE,TEL) VALUES (?,?,?,?,?,?)");
            preparedStatement.setString(1, e.getName());
            preparedStatement.setString(2, e.getPassword());
            preparedStatement.setInt(3, e.getAge());
            preparedStatement.setString(4, e.getEmail());
            preparedStatement.setString(5, e.getExperince());
            preparedStatement.setString(6, e.getTelephone());


            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public boolean updateEmploee(Employee e) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE emploee SET NAME = ? , PASSWORD = ? ");
            preparedStatement.setString(1, e.getName());
            preparedStatement.setString(2, e.getPassword());

            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public boolean deleteEmploee(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM emploee where ID=? ");
            preparedStatement.setInt(1, id);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
