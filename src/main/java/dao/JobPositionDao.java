package dao;

import dao.DaoInterface.JobPositionInterface;
import dao.DbConnector.ConnectionFactory;
import model.JobPosition;
import controller.CompanyController;
import controller.JobPositionController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class JobPositionDao implements JobPositionInterface {
    public JobPosition getJOB_POSITION(int id) {
        Connection connection= ConnectionFactory.getConnection();
        try{
            PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM job_position WHERE ID=?");
            preparedStatement.setInt(1,id);
            ResultSet resultset = preparedStatement.executeQuery();
            if (resultset.next()){
                return ExtractJobPositionFromResultSet(resultset);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private JobPosition ExtractJobPositionFromResultSet(ResultSet resultset) throws SQLException {
        JobPosition jobPosition=new JobPosition();
        jobPosition.setId(resultset.getInt("ID"));
        jobPosition.setTitle(resultset.getString("TITLE"));
        jobPosition.setDesc(resultset.getString("DESCRIBTION"));
//        jobPosition.setViewCount(resultset.getInt("VIEW_COUNT"));
        jobPosition.setJobCategoryId(resultset.getInt("JOB_CATEGORY_ID"));
        jobPosition.setCompany_id(resultset.getInt("COMPAYNY_ID"));
        jobPosition.setContract_id(resultset.getInt("CONTRACT_TYPE_ID"));
        return jobPosition;
    }

    public ArrayList<JobPosition> getAllEmploees() {
        Connection connection= ConnectionFactory.getConnection();
        try{
            PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM job_position ");
            ResultSet resultset = preparedStatement.executeQuery();
            ArrayList<JobPosition> JobPositions=new ArrayList<>();
            if (resultset.next()) {
                JobPosition j = ExtractJobPositionFromResultSet(resultset);
                JobPositions.add(j);
            }
                return JobPositions;

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public boolean insertJOB_POSITION(JobPosition jobPosition) {
        Connection connection=ConnectionFactory.getConnection();

        try{
            PreparedStatement ps=connection.prepareStatement("INSERT INTO job_position values (null,?,?,?,?,?)");
            ps.setString(1,jobPosition.getTitle());
            ps.setString(2,jobPosition.getDesc());

            ps.setInt(3,jobPosition.getJobCategoryId());
            ps.setInt(4,jobPosition.getContract_id());
            ps.setInt(5, CompanyController.currentCompanyID);
            int i=ps.executeUpdate();
            if (i==1){
                return true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    public boolean updateJOB_POSITION(JobPosition jobPosition) {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("UPDATE job_position SET TITLE=? where ID=?");
            preparedStatement.setString(1,jobPosition.getTitle());
            preparedStatement.setInt(2, JobPositionController.currentJobId);

            int i=preparedStatement.executeUpdate();
            if (i==1){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteJOB_POSITION(int id) {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM company where ID=? ");
            preparedStatement.setInt(1,id);
            int i=preparedStatement.executeUpdate();
            if (i==1){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
