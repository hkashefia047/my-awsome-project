package dao;

import dao.DbConnector.ConnectionFactory;
import dao.DaoInterface.CompanyInterface;
import model.Company;
import controller.CompanyController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CompanyDao implements CompanyInterface {

    public Company getCompany(int id) {
        Connection connection= ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("select * from company where ID=?");
            preparedStatement.setInt(1,id);

            ResultSet rs=preparedStatement.executeQuery();
            if (rs.next()){
                return extractCompanyFromResultSet(rs);
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    private Company extractCompanyFromResultSet(ResultSet rs) throws SQLException {
        Company company=new Company();
        company.setId(rs.getInt("ID"));
        company.setName(rs.getString("USERNAME"));
        company.setPassword(rs.getString("PASSWORD"));
        company.setField(rs.getString("FIELD"));
        company.setDescribtion(rs.getString("DESCRIBTION"));
        company.setEmployeeCount(rs.getInt("EMPLOYEE_COUNT"));
        return company;
    }

    public ArrayList<Company> getAllCompany() {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("select * from company");
            ArrayList<Company> CompanySet=new ArrayList<>();
            ResultSet rs=preparedStatement.executeQuery();
            while (rs.next()){
                Company company=new Company();
                company.setId(rs.getInt("ID"));
                company.setName(rs.getString("USERNAME"));
                company.setPassword(rs.getString("PASSWORD"));
                company.setField(rs.getString("FIELD"));
                company.setDescribtion(rs.getString("DESCRIBTION"));
                company.setEmployeeCount(rs.getInt("EMPLOYEE_COUNT"));
                CompanySet.add(company);
            }
            return CompanySet;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public Company getCompanyByCompanyNameAndPassword(String name, String password) {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("SELECT * FROM company where USERNAME=? AND PASSWORD=?");
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,password);

            ResultSet resultSet=preparedStatement.executeQuery();

             if(resultSet.next()){
                 CompanyController.currentCompanyID= resultSet.getInt("ID");
                 return extractCompanyFromResultSet(resultSet);
             }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public boolean insertCompany(Company company) {
        Connection connection=ConnectionFactory.getConnection();
        try{
            PreparedStatement ps=connection.prepareStatement("INSERT INTO company(USERNAME,PASSWORD,FIELD,DESCRIBTION,EMPLOYEE_COUNT) VALUES (?,?,?,?,?)");
            ps.setString(1,company.getName());
            ps.setString(2,company.getPassword());
            ps.setString(3,company.getField());
            ps.setString(4,company.getDescribtion());
            ps.setInt(5,company.getEmployeeCount());

            int i=ps.executeUpdate();

            if (i==1){
                return true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    public boolean updateCompany(Company company) {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("UPDATE company SET NAME = ? , PASSWORD = ? WHERE ID=?");
            preparedStatement.setString(1,company.getName());
            preparedStatement.setString(2,company.getPassword());
            preparedStatement.setInt(3,company.getId());
            int i=preparedStatement.executeUpdate();
            if (i==1){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    public boolean deleteCompany(int id) {
        Connection connection=ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement=connection.prepareStatement("DELETE FROM company where ID=? ");
            preparedStatement.setInt(1,id);
              int i=preparedStatement.executeUpdate();
            if (i==1){
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }
}
