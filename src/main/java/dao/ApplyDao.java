package dao;

import dao.DaoInterface.ApplyInterface;
import dao.DbConnector.ConnectionFactory;
import model.Apply;
import controller.EmploeeController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ApplyDao implements ApplyInterface {
    @Override
    public Apply getApply(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from Apply where ID=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Apply apply=new Apply();
                apply.setId(resultSet.getInt("id"));
                apply.setEmployeeId(resultSet.getInt("Employee_id"));
                apply.setJobPositionId(resultSet.getInt("jobPosition_id"));
                return apply;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Apply> getAllApplys() {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from apply");
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Apply>a = new ArrayList<>();

            while (resultSet.next()) {

                Apply apply=new Apply();
                apply.setId(resultSet.getInt("id"));
                apply.setEmployeeId(resultSet.getInt("Employee_id"));
                apply.setJobPositionId(resultSet.getInt("jobPosition_id"));

                a.add(apply);
            }
            return a;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insertApply(Apply apply) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO apply(Employee_id,jobPosition_id) VALUES(?,?)");
            preparedStatement.setInt(1,apply.getEmployeeId());
            preparedStatement.setInt(2,apply.getJobPositionId());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateApply(Apply apply) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE apply SET Employee_id=? and jobPosition_id=?where ID=?");
            preparedStatement.setInt(1, apply.getEmployeeId());
            preparedStatement.setInt(2,apply.getJobPositionId());
            preparedStatement.setInt(3, EmploeeController.currentEmployeeId);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteApply(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE apply WHERE ID=?");
            preparedStatement.setInt(1, id);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
