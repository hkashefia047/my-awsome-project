package dao;

import dao.DaoInterface.ContractInterface;
import dao.DbConnector.ConnectionFactory;
import model.Contract;
import model.JobCategory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ContractDao implements ContractInterface {
    @Override
    public Contract getCONTRACT(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from contract-type where ID=?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Contract contract=new Contract();
                contract.setId(resultSet.getInt("ID"));
                contract.setType(resultSet.getString("TYPE"));
                return contract;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public ArrayList<Contract> getAllCONTRACT() {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from contract_type");
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<Contract>categories = new ArrayList<>();

            while (resultSet.next()) {

                Contract contract=new Contract();
                contract.setId(resultSet.getInt("ID"));
                contract.setType(resultSet.getString("ContractType"));

                categories.add(contract);
            }
                return categories;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean insertCONTRACT(Contract contract) {

        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO contract_type(ContractType) VALUES(?)");
            preparedStatement.setString(1,contract.getType());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean updateCONTRACT(Contract contract) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE contract_type SET ContractType=? where ID=?");
            preparedStatement.setString(1, contract.getType());
            preparedStatement.setInt(2,contract.getId());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean deleteCONTRACT(int id) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE contract_type WHERE ID=?");
            preparedStatement.setInt(1, id);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
