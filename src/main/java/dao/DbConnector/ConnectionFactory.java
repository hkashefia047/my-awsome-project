package dao.DbConnector;
import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    public static final String URL = "jdbc:mysql://localhost:3306/jobfinder";

    public static final String USER = "root";

    public static final String PASS = "@windows8";

    public static Connection getConnection()

    {

        try {

            return DriverManager.getConnection(URL, USER, PASS);

        } catch (SQLException ex) {

            throw new RuntimeException("Error connecting to the database", ex);

        }

    }

}
