package dao;

import dao.DbConnector.ConnectionFactory;
import dao.DaoInterface.CategoryInterface;
import model.JobCategory;


import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class CategoryDao implements CategoryInterface {

    public JobCategory getCategory(int ID) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from job_category where ID=?");
            preparedStatement.setInt(1, ID);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                JobCategory jobCategory = new JobCategory();
                jobCategory.setId(resultSet.getInt("ID"));
                jobCategory.setTitle(resultSet.getString("TITLE"));
                return jobCategory;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public ArrayList<JobCategory> getAllCategorys() {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from job_category");
            ResultSet resultSet = preparedStatement.executeQuery();

            ArrayList<JobCategory> categories = new ArrayList<>();

            while (resultSet.next()) {

                JobCategory jobCategory = new JobCategory();
                jobCategory.setId(resultSet.getInt("ID"));
                jobCategory.setTitle(resultSet.getString("TITLE"));

                categories.add(jobCategory);
            }
            return categories;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public boolean insertCategory(JobCategory jobCategory) {

        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO job_category (TITLE) VALUES(?)");
            preparedStatement.setString(1,jobCategory.getTitle());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public boolean updateCategory(JobCategory jobCategory) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE job_category SET TITLE=? WHERE ID=?");
            preparedStatement.setString(1, jobCategory.getTitle());
            preparedStatement.setInt(2,jobCategory.getId());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }


    public boolean deleteCategory(int ID) {
        Connection connection = ConnectionFactory.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM job_category WHERE ID=?");
            preparedStatement.setInt(1, ID);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
